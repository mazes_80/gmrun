/*****************************************************************************
 *	$Id: gtkcompletionline.h
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon									Email: mishoo@fenrir.infoiasi.ro
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#ifndef __GTKCOMPLETIONLINE_H__
#define __GTKCOMPLETIONLINE_H__

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#ifdef HAVE_GTK3
#include <glib/gprintf.h>
#else
#include <gdk/gdkkeysyms.h>
#endif

#include "history.h"

#define GTK_COMPLETION_LINE(obj) \
	G_TYPE_CHECK_INSTANCE_CAST(obj, gtk_completion_line_get_type(), GtkCompletionLine)
#define GTK_COMPLETION_LINE_CLASS(klass) \
	G_TYPE_CHECK_CLASS_CAST(klass, gtk_completion_line_get_type(), GtkCompletionLineClass)
#define IS_GTK_COMPLETION_LINE(obj) \
	G_TYPE_CHECK_INSTANCE_TYPE(obj, gtk_completion_line_get_type())

typedef struct _GtkCompletionLine GtkCompletionLine;
typedef struct _GtkCompletionLineClass GtkCompletionLineClass;

typedef enum
{
	GCL_SEARCH_OFF = 0,
	GCL_SEARCH_REW = 1,
	GCL_SEARCH_FWD = 2,
	GCL_SEARCH_BEG = 3
} GCL_SEARCH_MODE;

struct _GtkCompletionLine
{
	GtkEntry parent;
	GtkWidget *win_compl;
	GtkListStore *list_compl;
	GtkTreeModel *sort_list_compl;
	GtkWidget *tree_compl;
	GtkTreeIter list_compl_it;
	int list_compl_nr_rows; /* completion list size */
	int pos_in_text; /* Cursor position in main "line" */

	GList *cmpl; /* Completion list */
	GList *where; /* current row pointer ??? */

	//HistoryFile *hist;
	GCL_SEARCH_MODE hist_search_mode;
	GString *hist_word;

	int first_key;
	int tabtimeout;
	int show_dot_files;
};

struct _GtkCompletionLineClass
{
	GtkEntryClass parent_class;
	/* add your CLASS members here */

	void (* unique)(GtkCompletionLine *cl);
	void (* notunique)(GtkCompletionLine *cl);
	void (* incomplete)(GtkCompletionLine *cl);
	void (* runwithterm)(GtkCompletionLine *cl);
	void (* search_mode)(GtkCompletionLine *cl);
	void (* search_letter)(GtkCompletionLine *cl);
	void (* search_not_found)(GtkCompletionLine *cl);
	void (* ext_handler)(GtkCompletionLine *cl);
	void (* cancel)(GtkCompletionLine *cl);
};

GtkWidget *gtk_completion_line_new();

void gtk_completion_line_last_history_item(GtkCompletionLine*);


#endif /* __GTKCOMPLETIONLINE_H__ */
/* vim: set noexpandtab: */

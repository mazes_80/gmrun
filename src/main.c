/*****************************************************************************
 *	$Id: main.c
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#ifdef USE_POPT
#include <popt.h>
#endif
#include <string.h>

#ifdef MTRACE
#include <mcheck.h>
#endif

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "config.h"
#include "gtkcompletionline.h"
#include "prefs.h"

/* defined in gtkcompletionline.c */
extern GType gtk_completion_line_get_type();

int get_words(GtkCompletionLine *object, GList **words);

typedef struct _app_window
{
	GtkWidget *w1;
	GtkWidget *w2;
} app_window;

/// BEGIN: TIMEOUT MANAGEMENT
static gint search_off_timeout(app_window *app_w);

static guint g_search_off_timeout_id = 0;

static void remove_search_off_timeout() {
	if (g_search_off_timeout_id) {
		g_source_remove(g_search_off_timeout_id);
		g_search_off_timeout_id = 0;
	}
}

static void add_search_off_timeout(guint32 timeout, app_window *app_w, GSourceFunc func) {
	remove_search_off_timeout();
	if (!func)
		func = (GSourceFunc)(search_off_timeout);
	g_search_off_timeout_id = g_timeout_add(timeout, func, app_w);
}
/// END: TIMEOUT MANAGEMENT

/* Notice: coloration management */
void label_set_with_prop(GtkLabel *label, const gchar *text, const char *prop) {
	if(prop==NULL)
		gtk_label_set_text(label, text);
	else {
		const gchar *color=">";
		gchar *msg;
		if(strcmp(prop, "notunique")==0)
			color = " color='blue'>";
		else if(strcmp(prop, "unique")==0)
			color = " color='green'>";
		else if(strcmp(prop, "notfound")==0)
			color = " color='red'>";
		msg = g_strconcat("<span", color, text, "</span>", NULL);
		gtk_label_set_markup(label, msg);
		g_free(msg);
	}
}

/* Run command line using system */
static void run_with_system(const gchar *command, app_window* app_w)
{
	gchar *cmd;
	cmd = g_strconcat(command, " &", NULL);
	int ret = system(cmd);
#ifdef DEBUG
	g_fprintf(stderr, "System exit code: %d\n", ret);
#endif
	if (ret != -1)
		gtk_main_quit();
	else {
		gchar *error;
		error = g_strconcat("ERROR :", strerror(errno), NULL);
#ifdef DEBUG
		g_fprintf(stderr, error);
#endif
		label_set_with_prop(GTK_LABEL(app_w->w1), error, "notfound");
		add_search_off_timeout(2000, app_w, NULL);
		g_free(error);
	}
	g_free(cmd);
}

#ifdef USE_SYSTEM
/* Run function */
static void run_the_command(const gchar* command, app_window* app_w) {
	run_with_system(command, app_w);
}
#else
/* Run command line using execvp */
/* Does not give the hand back before launched app ends when ran from terminal */
/* Just keep it when --enable-system=no configure switch is not recommanded */
static void run_the_command(const gchar* command, app_window* app_w) {
	GList *argv = NULL, *argv_i;
	const gchar *cmd = g_strconcat(command, " ", NULL);
	const gchar *cmd_it = cmd;
#ifdef DEBUG
	g_fprintf(stderr, "%s\n", cmd);
#endif
	char what_quote = '"';
	typedef enum {
		CT_NORMAL = 0,
		CT_QUOTE,
		CT_ESCAPE
	} TYPE_CONTEXT;
	TYPE_CONTEXT context = CT_NORMAL;
	TYPE_CONTEXT save_context = CT_NORMAL;
	GString *tmp = g_string_new("");
	GString *prog = g_string_new("");
	char c;

	while (*cmd_it != '\0') {
		c = *cmd_it;
		switch (c) {
		case '\\':
			if (context == CT_ESCAPE)
				goto ordinary;
			save_context = context;
			context = CT_ESCAPE;
			break;
		case '\'':
		case '"':
			if (context == CT_ESCAPE)
				goto ordinary;
			if (context == CT_QUOTE) {
				if (what_quote != c)
					goto ordinary;
				context = CT_NORMAL;
			} else {
				context = CT_QUOTE;
				what_quote = c;
			}
			break;
		case ' ':
			if (context == CT_ESCAPE || context == CT_QUOTE)
				goto ordinary;
			if (tmp->len != 0) {
				if (prog->len == 0)
					prog = g_string_assign(prog, tmp->str);
				argv = g_list_append(argv, g_strdup(tmp->str));
				tmp = g_string_set_size(tmp, 0);
			}
			break;
		ordinary:
		default:
			if (context == CT_ESCAPE) {
				context = save_context;
				tmp = g_string_append_c(tmp, c);
			} else if (context == CT_QUOTE) {
				tmp = g_string_append_c(tmp, c);
			} else if (c != ' ') {
				tmp = g_string_append_c(tmp, c);
			}
		}
		cmd_it++;
	}
	gchar **argv_a = (gchar **)g_malloc(g_list_length(argv) * sizeof(gchar *) + sizeof(gchar *));
	int a_i = 0;
	argv_i = argv;

#ifdef DEBUG
	g_fprintf(stderr, prog->str);
#endif
	while(argv_i) {
		if(argv_i->data) {
			argv_a[a_i++] = (gchar *)argv_i->data;
#ifdef DEBUG
			g_fprintf(stderr, " '%s'", (gchar *)(argv_i->data));
#endif
		}
		argv_i = argv_i->next;
	}
#ifdef DEBUG
	g_fprintf(stderr, "\n");
#endif
	argv_a[a_i] = NULL;
	g_list_free(argv);

	execvp(prog->str, argv_a);
	gchar *error = g_strconcat("ERROR : ", strerror(errno), NULL);
#ifdef DEBUG
	g_fprintf(stderr, error);
#endif

	label_set_with_prop(GTK_LABEL(app_w->w1), error, "notfound");
	add_search_off_timeout(2000, app_w, NULL);
	g_string_free(prog, TRUE);
	g_string_free(tmp, TRUE);
	g_strfreev(argv_a);
	g_free(error);
}
#endif

/* Notice: Current extension handler info */
static void on_ext_handler(GtkCompletionLine *cl, const char* filename, app_window* app_w) {
	gchar *mime_type, *msg;
	const gchar *handler;
	GAppInfo *app_info;

	if (filename) {
		mime_type = g_content_type_guess(filename, NULL, 0, NULL);
		if(mime_type) {
			app_info = g_app_info_get_default_for_type (g_content_type_get_mime_type(mime_type), FALSE);
			if(app_info) {
				handler = g_app_info_get_commandline (app_info);
				msg = g_strconcat("Handler: ", handler, NULL);
				gtk_label_set_text(GTK_LABEL(app_w->w2), msg);
				gtk_widget_show(app_w->w2);

				g_object_unref(app_info);
				g_free(msg);
				return;
			}
		}
	}
	search_off_timeout(app_w);
}

/* Notice: something ran through terminal */
static void on_compline_runwithterm(GtkCompletionLine *cl, app_window* app_w)
{
	gchar *command = g_locale_from_utf8 (gtk_entry_get_text(GTK_ENTRY(cl)),
					-1, NULL, NULL, NULL);
	gchar *tmp;
	unsigned int i = 0;

	/* Find first non-blank char to execute */
	while(command[i] == ' ' || command[i] == '\t') i++;
	if (command[i] != '\0') {
		tmp = g_strconcat(g_key_file_get_string(configuration, "Terminal", "command", NULL),
				" ", g_key_file_get_string(configuration, "Terminal", "exec", NULL),
				" '", command, "'", NULL);
#ifdef DEBUG
		g_fprintf(stderr);
#endif
		history_list_append(command);
		history_file_overwrite();
		run_with_system(tmp, app_w);
		g_free(tmp);
	}
	g_free(command);
}

static gint search_off_timeout(app_window *app_w)
{
	gtk_label_set_text(GTK_LABEL(app_w->w1), "Run program:");
	gtk_widget_hide(app_w->w2);
	g_search_off_timeout_id = 0;
	return FALSE;
}
/* Display error messages */
static void on_compline_unique(GtkCompletionLine *cl, app_window *app_w) {
	label_set_with_prop(GTK_LABEL(app_w->w1), "unique", "unique");
	add_search_off_timeout(1000, app_w, NULL);
}

static void on_compline_notunique(GtkCompletionLine *cl, app_window *app_w) {
	label_set_with_prop(GTK_LABEL(app_w->w1), "not unique", "notunique");
	add_search_off_timeout(1000, app_w, NULL);
}

static void on_compline_incomplete(GtkCompletionLine *cl, app_window *app_w) {
	label_set_with_prop(GTK_LABEL(app_w->w1), "Not found", "notfound");
	add_search_off_timeout(1000, app_w, NULL);
}
/* END: Display error messages */

/* Notice search mode set and displays/refresh the current search field */
static void on_search_mode(GtkCompletionLine *cl, app_window *app_w)
{
	if (cl->hist_search_mode != GCL_SEARCH_OFF) {
		gtk_widget_show(app_w->w2);
		gtk_label_set_text(GTK_LABEL(app_w->w1), "Search:");
		gtk_label_set_text(GTK_LABEL(app_w->w2), cl->hist_word->str);
	} else {
		gtk_widget_hide(app_w->w2);
		gtk_label_set_text(GTK_LABEL(app_w->w1), "Search OFF");
		add_search_off_timeout(1000, app_w, NULL);
	}
}

static void on_search_letter(GtkCompletionLine *cl, GtkWidget *label)
{
	gtk_label_set_text(GTK_LABEL(label), cl->hist_word->str);
}

static gint search_fail_timeout(app_window *app_w)
{
	label_set_with_prop(GTK_LABEL(app_w->w1), "Search:", "notunique");
	g_search_off_timeout_id = 0;
	return FALSE;
}

/* Display a message when search field is not found */
static void on_search_not_found(GtkCompletionLine *cl, app_window *app_w)
{
	label_set_with_prop(GTK_LABEL(app_w->w1), "Not found", "notfound");
	if(cl->hist_search_mode == GCL_SEARCH_FWD)
		history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),FALSE);
	else
		history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),TRUE);
	add_search_off_timeout(1000, app_w, (GSourceFunc)(search_fail_timeout));
}

/* Remove xdg desktop files fields from app and catenate args */
gchar *commandline_strip_concat(GAppInfo *app, const gchar *args) {
	gchar *cmd, *cmd_strip;
	GRegex *regex;

	regex = g_regex_new (".%[fFuUdDnNickvm]", G_REGEX_OPTIMIZE, G_REGEX_MATCH_NOTEMPTY, NULL);
	cmd = g_regex_replace_literal (regex,
			g_app_info_get_commandline(app), -1, 0, "", G_REGEX_MATCH_NOTEMPTY, NULL);
	cmd_strip = g_strconcat(cmd, " ", args, NULL);

	g_regex_unref(regex);
	g_free(cmd);
	return cmd_strip;
}

/* Handler for URLs  */
static gboolean url_check(GtkCompletionLine *cl, app_window *app_w) {
	gchar *text = g_locale_from_utf8 (gtk_entry_get_text(GTK_ENTRY(cl)), -1, NULL, NULL, NULL);
	gchar **words, **scheme;
	GAppInfo *handler_info;
	gchar *cmd;
	/* Process this text, ensure first word is an url, find the handler, and run the command */
	/* split the string and work on the first word */
	words = g_strsplit_set(text, " \t", -1);
	if(words && g_strv_length(words) > 0) {
		scheme = g_strsplit(words[0], ":", -1);
		/* check first word is an URL */
		if(g_strv_length(scheme) == 2 && scheme[1][0] == '/' && scheme[1][1] == '/') {
			/* Is there a known uri handler */
			handler_info = g_app_info_get_default_for_uri_scheme (scheme[0]);
			if(handler_info) {
				cmd = commandline_strip_concat(handler_info, words[0]);
				history_list_append(words[0]);
				history_file_overwrite();
				run_the_command(cmd, app_w);
				g_object_unref(handler_info);
			}
			else {
				cmd = g_strconcat("No URL handler for [", scheme[0], "]", NULL);
				label_set_with_prop(GTK_LABEL(app_w->w1), cmd, "notfound");
				add_search_off_timeout(1000, app_w, NULL);
			}
			g_strfreev(scheme);
			g_strfreev(words);
			g_free(cmd);
			return TRUE;
		}
		g_strfreev(scheme);
	}
	g_strfreev(words);

	return FALSE;
}

/* Handler for extensions */
static gboolean ext_check(GtkCompletionLine *cl, app_window *app_w) {
	GList *words = NULL;
	get_words(cl, &words);
	words = g_list_first(words);
	gchar *quoted, *mime_type, *cmd;
	GAppInfo *app_info;
	GRegex *regex;
	gboolean sure;

	regex = g_regex_new (" ", G_REGEX_OPTIMIZE, G_REGEX_MATCH_NOTEMPTY, NULL);
	/* Launches the first entry if executable or open through xdg if known mime type */
	while (words) {
		quoted = g_regex_replace_literal (regex, (gchar *)words->data, -1, 0, "\\ ", G_REGEX_MATCH_NOTEMPTY, NULL);
		words = words->next;
		/* File is executable: launch it (fail silently if file isn't really executable) */
		if (g_file_test (quoted, G_FILE_TEST_IS_EXECUTABLE)) {
			run_the_command(quoted, app_w);
			g_list_free_full(words, g_free);
			g_free(quoted);
			return TRUE;
		}
		/* Check mime type through extension */
		if (quoted[0] == '/') {
			mime_type = g_content_type_guess(quoted, NULL, 0, &sure);
			if(mime_type) {
				app_info = g_app_info_get_default_for_type (g_content_type_get_mime_type(mime_type), FALSE);
				if(app_info) {
					cmd = commandline_strip_concat(app_info, quoted);
					/* Launch the command if found mime */
					run_the_command(cmd, app_w);

					g_list_free_full(words, g_free);
					g_object_unref(app_info);
					g_free(quoted);
					g_free(cmd);
					return TRUE;
				}
			}
			g_free(mime_type);
		}
		g_free(quoted);
		// TODO: check next entries
		break;
	}
	g_list_free_full(words, g_free);
	g_regex_unref(regex);

	return FALSE;
}

/* The user pressed 'enter' */
static void on_compline_activated(GtkCompletionLine *cl, app_window *app_w) {
	if (url_check(cl, app_w))
		return;
	if (ext_check(cl, app_w))
		return;

	gchar *command = g_locale_from_utf8 (gtk_entry_get_text(GTK_ENTRY(cl)),
							-1, NULL, NULL, NULL);
	gchar **term_progs;
	gsize term_progs_length, t_i=0;
	unsigned int i=0, j=0;

	/* Seek for first non blank char and first word end */
	while(command[i] == ' ' || command[i] == '\t') i++;
	if(command[i] == '\0') {
		g_free(command);
		return;
	}
	j = i; /* i = word first letter position */
	while(command[j] != ' ' && command[j] != '\t' && command[j] != '\0') j++;
	j = j - i; /* j = word length */

	term_progs = g_key_file_get_string_list(configuration, "Terminal", "always_in_terminal", &term_progs_length, NULL);
#ifdef DEBUG
	g_fprintf(stderr, "---");
#endif
	while(term_progs[t_i]) {
#ifdef DEBUG
	g_fprintf(stderr, "%s\n", term_progs[i]);
#endif
		if( strlen(term_progs[t_i]) == j &&
			strncmp(command+i, term_progs[t_i], j) == 0)
		{
			on_compline_runwithterm(cl, app_w);
			return;
		}
		t_i++;
	}
	g_strfreev(term_progs);
#ifdef DEBUG
	g_fprintf(stderr, "---");
#endif
	history_list_append(command);
	history_file_overwrite();
	run_the_command(command, app_w);
	g_free(command);
}

int main(int argc, char **argv)
{
	GtkWidget *win;
	GtkWidget *compline;
	GtkWidget *label_search;
	app_window app_w;

#ifdef MTRACE
	mtrace();
#endif

	gtk_init(&argc, &argv);

	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_realize(win);
	gdk_window_set_decorations(gtk_widget_get_window(win), GDK_DECOR_BORDER);
	gtk_widget_set_name(win, "gmrun");
	gtk_window_set_title(GTK_WINDOW(win), "A simple launcher with completion");
	gtk_window_set_resizable(GTK_WINDOW(win), TRUE);
	gtk_container_set_border_width(GTK_CONTAINER(win), 4);
	g_signal_connect(G_OBJECT(win), "destroy",
										G_CALLBACK(gtk_main_quit), NULL);

#ifdef HAVE_GTK3
	GtkWidget *hbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
	GtkWidget *hhbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2);
#else
	GtkWidget *hbox = gtk_vbox_new(FALSE, 2);
	GtkWidget *hhbox = gtk_hbox_new(FALSE, 2);
#endif
	gtk_widget_show(hbox);
	gtk_container_add(GTK_CONTAINER(win), hbox);

	gtk_widget_show(hhbox);
	gtk_box_pack_start(GTK_BOX(hbox), hhbox, FALSE, FALSE, 0);

	GtkWidget *label = gtk_label_new("Run program:");
	gtk_widget_show(label);
#ifdef HAVE_GTK3
	gtk_label_set_xalign(GTK_LABEL(label), 0.0);
	gtk_label_set_yalign(GTK_LABEL(label), 1.0);
	gtk_widget_set_margin_start(GTK_WIDGET(label),10);
	gtk_widget_set_margin_end(GTK_WIDGET(label),10);
#else
	gtk_misc_set_alignment(GTK_MISC(label), 0.0, 1.0);
	gtk_misc_set_padding(GTK_MISC(label), 10, 0);
#endif
	gtk_box_pack_start(GTK_BOX(hhbox), label, FALSE, FALSE, 0);

	label_search = gtk_label_new("");
	gtk_widget_show(label_search);
#ifdef HAVE_GTK3
	gtk_label_set_xalign(GTK_LABEL(label_search), 1.0);
	//gtk_label_set_yalign(GTK_LABEL(label_search), 0.5);
	gtk_widget_set_margin_start(GTK_WIDGET(label_search),10);
	gtk_widget_set_margin_end(GTK_WIDGET(label_search),10);
#else
	gtk_misc_set_alignment(GTK_MISC(label_search), 1.0, 0.5);
	gtk_misc_set_padding(GTK_MISC(label_search), 10, 0);
#endif
	gtk_box_pack_start(GTK_BOX(hhbox), label_search, TRUE, TRUE, 0);

	prefs_init();
	compline = gtk_completion_line_new();
	gtk_widget_set_name(compline, "Completion line widget");
	int prefs_width = g_key_file_get_integer(configuration, "Window", "width", NULL);

	/* do not show hidden files by default */
	if(!g_key_file_get_boolean(configuration, "Completion", "show_hidden", NULL))
		GTK_COMPLETION_LINE(compline)->show_dot_files = 0;
	else
		GTK_COMPLETION_LINE(compline)->show_dot_files = 1;

	GTK_COMPLETION_LINE(compline)->tabtimeout = g_key_file_get_integer(configuration, "Completion", "timeout", NULL);

	app_w.w1 = label;
	app_w.w2 = label_search;

	gtk_widget_set_size_request(compline, prefs_width, -1);
#ifdef HAVE_GTK3
	gtk_window_set_default_size(GTK_WINDOW(win), prefs_width, 40);
#endif
	g_signal_connect(G_OBJECT(compline), "cancel",
										G_CALLBACK(gtk_main_quit), NULL);
	g_signal_connect(G_OBJECT(compline), "activate",
										G_CALLBACK(on_compline_activated), &app_w);
	g_signal_connect(G_OBJECT(compline), "runwithterm",
										G_CALLBACK(on_compline_runwithterm), &app_w);
	g_signal_connect(G_OBJECT(compline), "unique",
										G_CALLBACK(on_compline_unique), &app_w);
	g_signal_connect(G_OBJECT(compline), "notunique",
										G_CALLBACK(on_compline_notunique), &app_w);
	g_signal_connect(G_OBJECT(compline), "incomplete",
										G_CALLBACK(on_compline_incomplete), &app_w);
	g_signal_connect(G_OBJECT(compline), "search_mode",
										G_CALLBACK(on_search_mode), &app_w);
	g_signal_connect(G_OBJECT(compline), "search_not_found",
										G_CALLBACK(on_search_not_found), &app_w);
	g_signal_connect(G_OBJECT(compline), "search_letter",
										G_CALLBACK(on_search_letter), label_search);
	g_signal_connect(G_OBJECT(compline), "ext_handler",
										G_CALLBACK(on_ext_handler), &app_w);
	gtk_widget_show(compline);

	if (g_key_file_get_boolean(configuration, "History", "show", NULL)) {
		gtk_completion_line_last_history_item(GTK_COMPLETION_LINE(compline));
	}

	gtk_box_pack_start(GTK_BOX(hbox), compline, TRUE, TRUE, 0);

	int prefs_top = g_key_file_get_integer(configuration, "Window", "top", NULL);

	// parse commandline options
	char *geoptr = NULL;

#ifdef USE_POPT
	poptContext context;
	struct poptOption options[] = {
		{ "geometry", 'g', POPT_ARG_STRING | POPT_ARGFLAG_ONEDASH,
			&geoptr, 0, "This option specifies the initial "
			"size and location of the window.", NULL },
		POPT_AUTOHELP
		{ NULL, '\0', 0, NULL, 0 }
	};

	context = poptGetContext("popt1", argc, (const char**) argv, options, 0);
	poptGetNextOpt(context);
#else
	GOptionEntry entries[] =
	{
		{ "geometry", 'g', 0, G_OPTION_ARG_STRING, &geoptr, "This option specifies the initial size and location of the window.", NULL },
		{ NULL }
	};
	GError *error = NULL;
	GOptionContext *context;

	context = g_option_context_new ("- test tree model performance");
	g_option_context_add_main_entries (context, entries, NULL);
	g_option_context_add_group (context, gtk_get_option_group (TRUE));
	if (!g_option_context_parse (context, &argc, &argv, &error))
	{
		g_print ("option parsing failed: %s\n", error->message);
		exit (1);
	}
	g_free(context);
	g_free(error);
#endif

	if ( geoptr == NULL )
		gtk_window_move(GTK_WINDOW (win), prefs_top, prefs_top);
	else {
		gchar **splitted_geometry;
		GRegex *geometry;
		const gchar *georegex="^(((\\d+)x(\\d+))?(([\\+\\-]\\d+)([\\+\\-]\\d+))?)$";
		int offset=0, pos_x, pos_y;

		if(g_str_has_prefix(geoptr, "+") || g_str_has_prefix(geoptr, "-")) {
				georegex="^([\\+\\-]\\d+)([\\+\\-]\\d+)$";
				offset=1;
		}

		geometry = g_regex_new(georegex, G_REGEX_CASELESS, G_REGEX_MATCH_NOTEMPTY, NULL );
		splitted_geometry = g_regex_split_full( geometry, geoptr, -1, 0, G_REGEX_MATCH_NOTEMPTY, 8, NULL);
		g_regex_unref(geometry);

		if(!offset) {
			gtk_window_set_default_size(GTK_WINDOW (win),
					g_ascii_strtoll(splitted_geometry[3],NULL, 0),
					g_ascii_strtoll(splitted_geometry[4],NULL, 0));
			if(splitted_geometry[6] && splitted_geometry[7]) {
				offset = 6;
			}
		}
		if(offset) {
			// TODO check what it does return in case of multiple monitor
			// documentation does not state:
			//		display with mouse pointer
			//		primary
			//		with current opened window ???
			GdkDisplay *display = gdk_display_get_default();
			gint w_w, w_h;
			#ifdef HAVE_GTK3
			// Get monitor for the default display
			GdkMonitor *monitor = gdk_display_get_monitor (display, gdk_display_get_n_monitors(display));
			GdkRectangle r;
			gdk_monitor_get_geometry(monitor, &r);
			#else
			GdkScreen *screen = gdk_display_get_default_screen (display);
			#endif
			gtk_window_get_default_size (GTK_WINDOW(win), &w_w, &w_h);

			pos_x = g_ascii_strtoll(splitted_geometry[offset],NULL, 0);
			if(g_str_has_prefix(splitted_geometry[offset], "-")) {
				#ifdef HAVE_GTK3
				pos_x = r.width  + pos_x - w_w - 1;
				#else
				pos_x = gdk_screen_get_width(screen) + pos_x - w_w - 1;
				#endif
			}
			pos_y = g_ascii_strtoll(splitted_geometry[offset+1],NULL, 0);
			if(g_str_has_prefix(splitted_geometry[offset+1], "-")) {
				#ifdef HAVE_GTK3
				pos_y = r.height + pos_y - w_h - 1;
				#else
				pos_y = gdk_screen_get_height(screen) + pos_y - w_h - 1;
				#endif
			}
			gtk_window_move(GTK_WINDOW(win), pos_x, pos_y);
		}
		g_strfreev(splitted_geometry);
	}

	gtk_widget_show(win);

	gtk_window_set_focus(GTK_WINDOW(win), compline);

	gtk_main();
}

/* vim: set noexpandtab: */

/*****************************************************************************
 *  $Id: prefs.h
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#ifndef __PREFS_H__
#define __PREFS_H__

#include <glib.h>

extern GKeyFile *configuration;

void prefs_init();

#endif /* __PREFS_H__ */
/* vim: set noexpandtab: */

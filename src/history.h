/*****************************************************************************
 *  $Id: history.h
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#ifndef __HISTORY_H__
#define __HISTORY_H__

#include <glib.h>
extern GList *history;

void history_file_init();
void history_file_overwrite();
void history_list_append(const char *);
void history_set_default(const char *);
void history_clear_default();
void history_reset_position(const gchar *, gboolean);
const char *history_prev();
const char *history_next();
const char *history_prev_to_first();
const char *history_next_to_last();
const char *history_last_item();
//const char *history_first_item();
#endif // __HISTORY_H__
/* vim: set noexpandtab: */

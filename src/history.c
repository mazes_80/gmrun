/*****************************************************************************
 * $Id: history.c
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#include <gio/gio.h>
#include <glib/gprintf.h>
#include <stdio.h>
#include <string.h>


#include "history.h"
#include "prefs.h"

static gchar *m_filename;
static const gchar *m_default;
static gboolean m_default_set;
static GList *l_current;
static int HIST_MAX_SIZE;
GList *history;

static void history_file_read();
static void history_list_prune();

void history_file_init() {
	m_filename = g_strconcat(g_get_user_config_dir(), "/gmrun_history", NULL);
	m_default_set = FALSE;
	history = NULL;
	HIST_MAX_SIZE = g_key_file_get_integer(configuration, "History", "lines", NULL);
	history_file_read();
	if(g_key_file_get_boolean(configuration, "History", "empty_end", NULL)) {
		history = g_list_append(history, "");
	}
	l_current = g_list_last(history);
}

static void history_file_read() {
	GFile *f = g_file_new_for_path(m_filename);
	GFileInputStream *gfis = NULL;
	GDataInputStream *gdis = NULL;
	GError *err = NULL;
	char *line;
	gsize length;

	GFileInfo *info;

	int total_size = -1;

	/* get input stream */
	gfis = g_file_read(f, NULL, &err);

	if (err != NULL) {
		g_fprintf(stderr, "ERROR: opening %s\n", m_filename);
		g_object_unref(f);
		return;
	}

	info = g_file_input_stream_query_info (G_FILE_INPUT_STREAM (gfis),G_FILE_ATTRIBUTE_STANDARD_SIZE,NULL, &err);
	if (info)
	{
		if (g_file_info_has_attribute (info, G_FILE_ATTRIBUTE_STANDARD_SIZE))
			total_size = g_file_info_get_size (info);
		g_object_unref (info);
	}
	if(total_size > 0) {
		gdis = g_data_input_stream_new(G_INPUT_STREAM(gfis));
		/* TODO check possible errors */
		while((line = g_data_input_stream_read_line_utf8 (gdis, &length, NULL, &err))) {
			history = g_list_append(history, line);
		}
		history_list_prune();
	}

	g_object_unref(gdis);
	g_object_unref(gfis);
	g_object_unref(f);
}

static gchar *history_list_get_datas() {
	GString *data;
	GList *history_it;

	if(!history) return NULL;
	history_list_prune();
	history_it = g_list_first(history);
	data = g_string_new((gchar *)(history_it->data));
	while((history_it = history_it->next)) {
		if(strlen((gchar *)(history_it->data)) == 0)
			continue;
		data = g_string_append_c(data,'\n');
		data = g_string_append(data, (gchar *)(history_it->data));
	}
	return g_string_free(data, FALSE);
}

/* Write to history file */
void history_file_overwrite() {
	gchar *data = history_list_get_datas();

	if(!data) return;
	if(!g_file_set_contents (m_filename, data, -1, NULL)) {
		g_fprintf(stderr, "Can not write history to: %s\n", m_filename);
	}
	g_free(data);
}

/* Prune history list begin when larger than allowed */
static void history_list_prune() {
	while(g_list_length(history) > (guint)HIST_MAX_SIZE) {
		g_free(history->data);
		history = g_list_delete_link(history, history);
	}
}

/* appends last command to the history list */
void history_list_append(const char *data) {
	GList *history_it;

	if(!history) return;
	if(g_key_file_get_boolean(configuration, "History", "ignorespace", NULL) && (data[0] == ' ' || data[0] == '\t'))
		return;
	history_it = g_list_last(history);
	while(history_it && strcmp((gchar *)history_it->data, data) != 0) {
		history_it = history_it->prev;
	}

	if(!history_it) { /* New command in list */
		history = g_list_append(history, g_strdup(data));
		history_list_prune();
	}
	else { /* Move saved command at the latest position */
		history = g_list_remove_link(history, history_it);
		history = g_list_concat(history, history_it);
	}

}

void history_set_default(const char *defstr) {
	if (!m_default_set) {
		m_default = defstr;
		m_default_set = TRUE;
	}
}

void history_clear_default() {
	m_default_set = FALSE;
}

const char *history_prev() {
	if(!history) return m_default;
	if(!l_current || !l_current->prev) l_current = g_list_last(history);
	else l_current = l_current->prev;
	if(!l_current) return m_default;
	return (const gchar *)l_current->data;
}

const char *history_next() {
	if(!history) return m_default;
	if(!l_current || !l_current->next) l_current = g_list_first(history);
	else l_current = l_current->next;
	return (const gchar *)l_current->data;
}

/* reset current entry index. Last version just reset to 0 */
void history_reset_position(const gchar *data, gboolean up) {
	if(!data) {
		l_current = history;
		return;
	}
	l_current = g_list_last(history);
	while(l_current) {
		if(strcmp(data, (gchar *)l_current->data) == 0)
			break;
		l_current = l_current->prev;
	}

	if(!l_current) {
		if(up)
			l_current = g_list_last(history);
		else
			l_current = g_list_first(history);
	}
}

const char * history_prev_to_first() {
	if(!history || !l_current)
		return NULL;
	l_current = l_current->prev;
	if(!l_current)
		return m_default;
	return (const gchar *)l_current->data;
}

const char * history_next_to_last() {
	if(!history || !l_current)
		return NULL;
	l_current = l_current->next;
	if(!l_current)
		return m_default;
	return (const gchar *)l_current->data;
}

const char * history_last_item() {
	if(!g_key_file_get_boolean(configuration, "History", "show", NULL))
		return m_default;
	if(g_key_file_get_boolean(configuration, "History", "empty_end", NULL))
		l_current = l_current->prev;
	if(!l_current)
		return m_default;
	return (const gchar *)l_current->data;
}

/* vim: set noexpandtab: */

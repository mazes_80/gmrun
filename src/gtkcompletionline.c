/*****************************************************************************
 *	$Id: gtkcompletionline.c
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include "gtkcompletionline.h"

G_DEFINE_TYPE(GtkCompletionLine, gtk_completion_line, GTK_TYPE_ENTRY);

/* GLOBALS */
static int on_cursor_changed_handler = 0;
static int on_key_press_handler = 0;

/* signals */
enum {
	UNIQUE,
	NOTUNIQUE,
	INCOMPLETE,
	RUNWITHTERM,
	SEARCH_MODE,
	SEARCH_LETTER,
	SEARCH_NOT_FOUND,
	EXT_HANDLER,
	CANCEL,
	LAST_SIGNAL
};

#define GEN_COMPLETION_OK	1
#define GEN_CANT_COMPLETE	2
#define GEN_NOT_UNIQUE		3

static guint gtk_completion_line_signals[LAST_SIGNAL];

static gchar **path_gc;   /* A string list (gchar *) containing each directory in PATH */
static GList *execs_gc;   /* A chained list of GString: executables from current path */
static GList *dirlist_gc; /* A chained list of GString: directories from current path */
static gchar *prefix;
static int g_show_dot_files;

static gboolean on_key_press(GtkCompletionLine *cl, GdkEventKey *event, gpointer data);

/* class_init */
static void gtk_completion_line_class_init(GtkCompletionLineClass *klass) {
	GtkWidgetClass *object_class;

	object_class = (GtkWidgetClass*)klass;

	gtk_completion_line_signals[UNIQUE] = g_signal_new("unique",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, unique),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[NOTUNIQUE] = g_signal_new("notunique",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, notunique),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[INCOMPLETE] = g_signal_new("incomplete",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, incomplete),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[RUNWITHTERM] = g_signal_new("runwithterm",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, runwithterm),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[SEARCH_MODE] = g_signal_new("search_mode",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, search_mode),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[SEARCH_NOT_FOUND] = g_signal_new("search_not_found",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, search_not_found),
						NULL, NULL, g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
	gtk_completion_line_signals[SEARCH_LETTER] = g_signal_new("search_letter",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, search_letter),
						NULL, NULL, NULL, G_TYPE_NONE, 0);
	gtk_completion_line_signals[EXT_HANDLER] = g_signal_new("ext_handler",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, ext_handler),
						NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
						G_TYPE_POINTER);
	gtk_completion_line_signals[CANCEL] = g_signal_new("cancel",
						G_TYPE_FROM_CLASS(object_class), G_SIGNAL_RUN_FIRST,
						G_STRUCT_OFFSET(GtkCompletionLineClass, ext_handler),
						NULL, NULL, g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE, 1,
						G_TYPE_POINTER);

	klass->unique = NULL;
	klass->notunique = NULL;
	klass->incomplete = NULL;
	klass->runwithterm = NULL;
	klass->search_mode = NULL;
	klass->search_letter = NULL;
	klass->search_not_found = NULL;
	klass->ext_handler = NULL;
	klass->cancel = NULL;
}

/* init */
static void gtk_completion_line_init(GtkCompletionLine *object) {
	/* Add object initialization / creation stuff here */
	object->where = NULL;
	object->cmpl = NULL;
	object->win_compl = NULL;
	object->list_compl = NULL;
	object->sort_list_compl = NULL;
	object->tree_compl = NULL;
	object->hist_search_mode = GCL_SEARCH_OFF;
	object->hist_word = g_string_new("");
	object->tabtimeout = 0;
	object->show_dot_files = 0;

	on_key_press_handler = g_signal_connect(G_OBJECT(object), "key_press_event",
							G_CALLBACK(on_key_press), NULL);
	object->first_key = 1;
	history_file_init();
}

/* set last item as current entry */
void gtk_completion_line_last_history_item(GtkCompletionLine* object) {
	/* TODO first find last entered text */
	history_set_default("");
	const char* txt = history_last_item();
	gtk_entry_set_text(GTK_ENTRY(object),
						g_locale_to_utf8 (txt, -1, NULL, NULL, NULL));
	gtk_editable_select_region(GTK_EDITABLE(object), 0, strlen(txt));
}

/* Get one word of a string: separator is space, but only unescaped spaces */
static const gchar *get_token(const gchar *str, GString *g_string) {
	gboolean escaped = FALSE;
	g_string = g_string_set_size(g_string, 0);
	while(*str != '\0') {
		if(escaped) {
			g_string = g_string_append_c(g_string, *str);
			escaped = FALSE;
		} else if (*str == '\\') {
			escaped = TRUE;
		} else if (isspace(*str)) {
			while(isspace(*str)) str++;
			//if(*str == '\0') str--;
			break;
		}
		else
			g_string = g_string_append_c(g_string, *str);
		str++;
	}
	return str;
}

/* get words before current edit position */
int get_words(GtkCompletionLine *object, GList **words) {
	const gchar *content = gtk_entry_get_text(GTK_ENTRY(object));
	const gchar *c_i = content;
	int pos = gtk_editable_get_position(GTK_EDITABLE(object));
	int n_w = 0;
	GString *tmp = g_string_new("");

	//if(pos != strlen(content))
	// insert a space after pos in content
	// TODO find if really needed ??
	// As everything works it doesn't seems

	while(*c_i != '\0') {
		c_i = get_token(c_i, tmp);
		*words = g_list_append(*words, g_strdup(tmp->str));
		if(c_i - content < pos && c_i != content && *c_i != '\0')
			n_w++;
	}
	g_string_free(tmp, TRUE);

	return n_w;
}

/* Replace words in the entry fields, return position of char after first 'pos' words */
int set_words(GtkCompletionLine *object, GList *words, int pos) {
	GRegex *regex;
	GString *tmp;

	if (pos == -1)
		pos = g_list_length(words) - 1;
	int cur = 0;

	regex = g_regex_new (" ", G_REGEX_OPTIMIZE, G_REGEX_MATCH_NOTEMPTY, NULL);
	tmp = g_string_new("");
	while (words) {
		gchar *quoted = g_regex_replace_literal (
				regex, (gchar *)(words->data), -1, 0, "\\ ", G_REGEX_MATCH_NOTEMPTY, NULL);
		tmp = g_string_append(tmp, quoted);
		if (words != g_list_last(words))
			tmp = g_string_append_c(tmp, ' ');
		if (!pos && !cur)
			cur = strlen(tmp->str); /* cur: length of string after inserting pos words */
		else
			--pos;
		g_free(quoted);
		words = words->next;
	}
	g_regex_unref (regex);

	if (g_list_length(words) == 1) {
		gchar *rev_dot = g_strrstr(tmp->str, ".");
		if (rev_dot && rev_dot != tmp->str)
			g_signal_emit_by_name(G_OBJECT(object), "ext_handler", tmp->str);
		else
			g_signal_emit_by_name(G_OBJECT(object), "ext_handler", NULL);
	}

	gtk_entry_set_text(GTK_ENTRY(object),
				g_locale_to_utf8 (tmp->str, -1, NULL, NULL, NULL));
	gtk_editable_set_position(GTK_EDITABLE(object), cur);
	g_string_free(tmp, TRUE);
	return cur;
}

/* Split path */
static void generate_path() {
	char *path_cstr = (char*)getenv("PATH");

	g_strfreev(path_gc);
	path_gc = g_strsplit(path_cstr, ":", -1);
}

/* Filter callback for scandir */
static int select_executables_only(const struct dirent* dent) {
	int len = strlen(dent->d_name);
	int lenp = strlen(prefix);

	if (dent->d_name[0] == '.') {
		if (!g_show_dot_files)
			return 0;
		if (dent->d_name[1] == '\0')
			return 0;
		if ((dent->d_name[1] == '.') && (dent->d_name[2] == '\0'))
			return 0;
	}
	if (dent->d_name[len - 1] == '~')
		return 0;
	if (lenp == 0)
		return 1;
	if (lenp > len)
		return 0;

	if (strncmp(dent->d_name, prefix, lenp) == 0)
		return 1;

	return 0;
}

/* Quicksort callback for scandir, compares two dirent */
int my_alphasort(const struct dirent** va, const struct dirent** vb) {
	const struct dirent** a = (const struct dirent**)va;
	const struct dirent** b = (const struct dirent**)vb;

	const char* s1 = (*a)->d_name;
	const char* s2 = (*b)->d_name;

	int l1 = strlen(s1);
	int l2 = strlen(s2);
	int result = strcmp(s1, s2);

	if (result == 0) return 0;

	if (l1 < l2) {
		int res2 = strncmp(s1, s2, l1);
		if (res2 == 0) return -1;
	} else {
		int res2 = strncmp(s1, s2, l2);
		if (res2 == 0) return 1;
	}

	return result;
}

/* Iterates though PATH and list all executables */
static void generate_execs() {
	gchar **path_gc_i = path_gc;

	while(*path_gc_i) {
		struct dirent **eps;
		int n = scandir(*path_gc_i, &eps, select_executables_only, my_alphasort);
		if (n >= 0) {
			for (int j = 0; j < n; j++) {
				execs_gc = g_list_append(execs_gc, g_string_new(eps[j]->d_name));
				free(eps[j]);
			}
			free(eps);
		}
		path_gc_i++;
	}
}

/* Set executable list as a completion_line widget attribute */
static int generate_completion_from_execs(GtkCompletionLine *object) {
	g_list_foreach(object->cmpl, (GFunc)g_string_free, NULL);
	g_list_free(object->cmpl);
	object->cmpl = execs_gc;
	execs_gc = NULL;

	return 0;
}

/* get the shortest common begin of two strings */
static gchar* get_common_part(const char *p1, const char *p2) {
	GString *tmp = g_string_new("");

	while (*p1 == *p2 && *p1 != '\0' && *p2 != '\0') {
		tmp = g_string_append_c(tmp, *p1);
		p1++;
		p2++;
	}

	return g_string_free(tmp, FALSE);
}

static int complete_common(GtkCompletionLine *object) {
	GList *ls = object->cmpl;
	GList *words = NULL;
	GList *word_i;
	int pos = get_words(object, &words);
	gchar *tmp;
	word_i = g_list_nth(words, pos);
	g_free(word_i->data);
	word_i->data = g_strdup(((GString*)ls->data)->str);

	ls = g_list_next(ls);
	while (ls != NULL) {
		/* Before migrating to C/glib, this (gchar *) may be leaking */
		tmp = get_common_part((gchar *)word_i->data, ((GString*)ls->data)->str);
		g_free(word_i->data);
		word_i->data = tmp;
		ls = g_list_next(ls);
		if(strlen(tmp) == 1)
			break;
	}

	set_words(object, words, pos);
	g_list_free_full(words, g_free);

	return 0;
}

/* list all subdirs in what, return if ok or not */
static int generate_dirlist(const char *what) {
	char *str = strdup(what);
	char *p = str + 1;
	char *filename = str;
	GString *dest = g_string_new("/");
	int n;

	/* Check path validity in what */
	while (*p != '\0') {
		dest = g_string_append_c(dest, *p);
		if (*p == '/') {
			DIR* dir = opendir(dest->str);
			if (!dir) {
				free(str);
				return GEN_CANT_COMPLETE;
			}
			closedir(dir);
			filename = p;
		}
		++p;
	}

	*filename = '\0';
	filename++;
	g_string_free(dest, TRUE);
	dest = g_string_new(str);
	dest = g_string_append_c(dest, '/');

	g_list_foreach(dirlist_gc, (GFunc)g_string_free, NULL);
	g_list_free(dirlist_gc);

	struct dirent **eps;
	GString *foo;
	g_free(prefix);
	prefix = g_strdup(filename);
	n = scandir(dest->str, &eps, select_executables_only, my_alphasort);
	if (n >= 0) {
		for (int j = 0; j < n; j++) {
			{
				foo = g_string_new(dest->str);
				foo = g_string_append(foo, eps[j]->d_name);
				struct stat filestatus;
				stat(foo->str, &filestatus);
				if (S_ISDIR(filestatus.st_mode)) foo = g_string_append(foo, "/");
				dirlist_gc = g_list_append(dirlist_gc, foo);
			}
			free(eps[j]);
		}
		free(eps);
	}

	g_string_free(dest, TRUE);
	free(str);
	return GEN_COMPLETION_OK;
}

/* Set directories list as completion_line widget attributes */
static int generate_completion_from_dirlist(GtkCompletionLine *object) {
	g_list_foreach(object->cmpl, (GFunc)g_string_free, NULL);
	g_list_free(object->cmpl);
	object->cmpl = dirlist_gc;
	dirlist_gc = NULL;

	return 0;
}

/* Expand tilde */
static int parse_tilda(GtkCompletionLine *object) {
	const gchar *text = gtk_entry_get_text(GTK_ENTRY(object));
	const gchar *match = g_strstr_len(text, -1, "~");
	if (match) {
	gint cur = match - text;
		if ((cur > 0) && (text[cur - 1] != ' '))
			return 0;
		if ((guint)cur < strlen(text) - 1 && text[cur + 1] != '/') {
			// FIXME: Parse another user's home
			// #include <pwd.h>
			// struct passwd *p;
			// p=getpwnam(username);
			// printf("%s\n", p->pw_dir);
		} else {
			gtk_editable_insert_text(GTK_EDITABLE(object),
					g_get_home_dir(), strlen(g_get_home_dir()), &cur);
			gtk_editable_delete_text(GTK_EDITABLE(object), cur, cur + 1);
		}
	}

	return 0;
}

static void complete_from_list(GtkCompletionLine *object) {
	parse_tilda(object);
	GList *words = NULL, *word_i;
	int pos = get_words(object, &words);
	word_i = g_list_nth(words, pos);

	g_free(prefix);
	prefix = g_strdup((gchar *)(word_i->data));

	/* Completion list is opened */
	if (object->win_compl != NULL) {
		int current_pos;
		gpointer data;
		gtk_tree_model_get(object->sort_list_compl, &(object->list_compl_it), 0, &data, -1);
		object->where=(GList *)data;
		g_free(word_i->data);
		word_i->data = g_strdup(((GString*)object->where->data)->str);
		current_pos = set_words(object, words, pos);

		GtkTreePath *path = gtk_tree_model_get_path(object->sort_list_compl, &(object->list_compl_it));
		gtk_tree_view_set_cursor(GTK_TREE_VIEW(object->tree_compl), path, NULL, FALSE);
		gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(object->tree_compl), path, NULL, TRUE, 0.5, 0.5);
		gtk_tree_path_free(path);

		gtk_editable_select_region(GTK_EDITABLE(object), object->pos_in_text, current_pos);
	} else {
		g_free(word_i->data);
		word_i->data = g_strdup(((GString*)object->where->data)->str);
		object->pos_in_text = gtk_editable_get_position(GTK_EDITABLE(object));
		int current_pos = set_words(object, words, pos);
		gtk_editable_select_region(GTK_EDITABLE(object),
									object->pos_in_text, current_pos);
		object->where = g_list_next(object->where);
	}
	g_list_free_full(words, g_free);
}

static void on_cursor_changed(GtkTreeView *tree, gpointer data) {
	GtkCompletionLine *object = GTK_COMPLETION_LINE(data);

	GtkTreeSelection *selection;
	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(object->tree_compl));
	gtk_tree_selection_get_selected (selection, &(object->sort_list_compl), &(object->list_compl_it));

	g_signal_handler_block(G_OBJECT(object->tree_compl),
								on_cursor_changed_handler);
	complete_from_list(object);
	g_signal_handler_unblock(G_OBJECT(object->tree_compl),
								on_cursor_changed_handler);
}

static void cell_data_func( GtkTreeViewColumn *col, GtkCellRenderer *renderer,
							GtkTreeModel *model, GtkTreeIter *iter, gpointer user_data) {
	gpointer data;
	gchar *str;

	gtk_tree_model_get(model, iter, 0, &data, -1);
	str = ((GString*)(((GList *)data)->data))->str;
	g_object_set(renderer, "text", str, NULL);
}

static int complete_line(GtkCompletionLine *object) {
	parse_tilda(object);
	GList *words = NULL;
	/* TODO get_words just to access first elt ??? */
	int pos = get_words(object, &words);

	g_free(prefix);
	words = g_list_nth(words, pos);
	prefix = g_strdup((gchar *)(words->data));
	g_list_free_full(words, g_free);
	/* TODO get_words just to access first elt ??? */

	g_show_dot_files = object->show_dot_files;
	if (prefix[0] != '/') {
		if (object->where == NULL) {
			generate_path();
			generate_execs();
			generate_completion_from_execs(object);
			object->where = NULL;
		}
	} else if (object->where == NULL) {
		generate_dirlist(prefix);
		generate_completion_from_dirlist(object);
		object->where = NULL;
	}

	if (object->cmpl != NULL) {
		complete_common(object);
		object->where = object->cmpl;
	}

	if (object->where != NULL) {
		if (object->win_compl != NULL) {
			gboolean valid =
				gtk_tree_model_iter_next(object->sort_list_compl, &(object->list_compl_it));
			if(!valid)
				gtk_tree_model_get_iter_first(object->sort_list_compl, &(object->list_compl_it));
		}
		complete_from_list(object);
	}

	GList *ls = object->cmpl;

	if (g_list_length(ls) == 1) {
		g_signal_emit_by_name(G_OBJECT(object), "unique");
		return GEN_COMPLETION_OK;
	} else if (g_list_length(ls) == 0 || ls == NULL) {
		g_signal_emit_by_name(G_OBJECT(object), "incomplete");
		return GEN_CANT_COMPLETE;
	} else if (g_list_length(ls) >	1) {
		g_signal_emit_by_name(G_OBJECT(object), "notunique");
		words = NULL;
		int pos = get_words(object, &words);
		words = g_list_nth(words, pos);

		if (strcmp((gchar *)(words->data),((GString*)ls->data)->str) == 0) {

			/* TODO leak: created once but never freed */
			if (object->win_compl == NULL) {
				object->win_compl = gtk_window_new(GTK_WINDOW_POPUP);
				gtk_widget_set_name(object->win_compl, "Completion Window");

				// TODO
				//gtk_window_set_default_size(GTK_WINDOW(object->win_compl), 200, 200);
				gtk_window_set_resizable(GTK_WINDOW(object->win_compl), TRUE);

				object->list_compl = gtk_list_store_new (2, G_TYPE_POINTER, G_TYPE_INT);
				object->sort_list_compl = gtk_tree_model_sort_new_with_model(GTK_TREE_MODEL(object->list_compl));
				object->tree_compl = gtk_tree_view_new_with_model(GTK_TREE_MODEL(object->sort_list_compl));
				g_object_unref(object->list_compl);
				g_object_unref(object->sort_list_compl);

				GtkTreeViewColumn *col;
				GtkCellRenderer *renderer;
				col = gtk_tree_view_column_new();
				gtk_tree_view_append_column(GTK_TREE_VIEW(object->tree_compl), col);
				renderer = gtk_cell_renderer_text_new();
				gtk_tree_view_column_pack_start(col, renderer, TRUE);
				gtk_tree_view_column_set_cell_data_func(col, renderer, cell_data_func, NULL, NULL);

				col = gtk_tree_view_column_new();
				gtk_tree_view_append_column(GTK_TREE_VIEW(object->tree_compl), col);
				gtk_tree_view_column_set_visible(col, FALSE);

				GtkTreeSelection *selection;
				selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(object->tree_compl));
				gtk_tree_selection_set_mode(selection, GTK_SELECTION_BROWSE);
				gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(object->tree_compl), FALSE);

				on_cursor_changed_handler = g_signal_connect(GTK_TREE_VIEW(object->tree_compl), "cursor-changed",
															G_CALLBACK(on_cursor_changed), object);

				g_signal_handler_block(G_OBJECT(object->tree_compl),
											on_cursor_changed_handler);

				GList *p = ls;
				object->list_compl_nr_rows = 0;
				while (p) {
					GtkTreeIter it;
					gtk_list_store_append(object->list_compl, &it);
					gtk_list_store_set (object->list_compl, &it, 0, p, 1, object->list_compl_nr_rows, -1);
					object->list_compl_nr_rows++;
					p = g_list_next(p);
				}

				GtkWidget *scroll = gtk_scrolled_window_new(NULL, NULL);
				gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scroll), GTK_SHADOW_OUT);
				gtk_widget_show(scroll);
				gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW (scroll),
																			GTK_POLICY_NEVER,
																			GTK_POLICY_AUTOMATIC);

				gtk_container_set_border_width(GTK_CONTAINER(object->tree_compl), 2);
				gtk_container_add(GTK_CONTAINER (scroll), object->tree_compl);

				gtk_tree_model_get_iter_first(object->sort_list_compl, &(object->list_compl_it));

				gtk_widget_show(object->tree_compl);

				GtkAllocation al;

				gtk_container_add(GTK_CONTAINER(object->win_compl), scroll);

				GdkWindow *top = gtk_widget_get_parent_window(GTK_WIDGET(object));
				int x, y;
				gtk_widget_get_allocation(GTK_WIDGET(object), &al);
				gdk_window_get_position(top, &x, &y);
				x += al.x;
				y += al.y + al.height;

				gtk_window_move(GTK_WINDOW(object->win_compl), x, y);
				gtk_widget_show_all(object->win_compl);

				GtkTreePath *path = gtk_tree_model_get_path(object->sort_list_compl, &(object->list_compl_it));
				gtk_tree_view_set_cursor(GTK_TREE_VIEW(object->tree_compl), path, NULL, FALSE);
				gtk_tree_path_free(path);

				gtk_tree_view_columns_autosize(GTK_TREE_VIEW(object->tree_compl));
				gtk_widget_get_allocation(object->tree_compl, &al);
				gtk_widget_set_size_request(scroll, al.width + 40, 150);

				g_signal_handler_unblock(G_OBJECT(object->tree_compl),
											on_cursor_changed_handler);
			}
			g_list_free_full(words, g_free);
			return GEN_COMPLETION_OK;
		}
		g_list_free_full(words, g_free);
		return GEN_NOT_UNIQUE;
	}

	return GEN_COMPLETION_OK;
}

GtkWidget * gtk_completion_line_new() {
	return GTK_WIDGET(g_object_new(gtk_completion_line_get_type(), NULL));
}

static void up_history(GtkCompletionLine* cl) {
	history_set_default(gtk_entry_get_text(GTK_ENTRY(cl)));
	gtk_entry_set_text(GTK_ENTRY(cl),
				g_locale_to_utf8 (history_prev(), -1, NULL, NULL, NULL));
}

static void down_history(GtkCompletionLine* cl) {
	history_set_default(gtk_entry_get_text(GTK_ENTRY(cl)));
	gtk_entry_set_text(GTK_ENTRY(cl),
				g_locale_to_utf8 (history_next(), -1, NULL, NULL, NULL));
}

static int search_history_func(GtkCompletionLine* cl, gboolean avance, gboolean begin, const char *(*history_func)(void)) {
	if (cl->hist_word->len > 0) {
		const char * histext;
		if (avance) {
			histext = history_func();
			if (histext == NULL) {
				g_signal_emit_by_name(G_OBJECT(cl), "search_not_found");
				return 0;
			}
		} else {
			histext = gtk_entry_get_text(GTK_ENTRY(cl));
		}
		while (TRUE) {
			int i = 0;
			gchar *s = g_strstr_len(histext, -1, cl->hist_word->str);
			if (s && !(begin && s != histext)) {
				const char *tmp = gtk_entry_get_text(GTK_ENTRY(cl));
				i = s - histext;
				if (!(avance && strcmp(tmp, histext) == 0)) {
					gtk_entry_set_text(GTK_ENTRY(cl),
					g_locale_to_utf8 (histext, -1, NULL, NULL, NULL));
					gtk_editable_select_region(GTK_EDITABLE(cl),
											i, i + cl->hist_word->len);
					g_signal_emit_by_name(G_OBJECT(cl), "search_letter");
					return 1;
				}
			}
			histext = history_func();
			if (histext == NULL) {
				g_signal_emit_by_name(G_OBJECT(cl), "search_not_found");
				break;
			}
		}
	} else {
		gtk_editable_select_region(GTK_EDITABLE(cl), 0, 0);
		g_signal_emit_by_name(G_OBJECT(cl), "search_letter");
	}

	return 0;
}

static int search_history(GtkCompletionLine* cl, gboolean avance, gboolean begin) {
	switch (cl->hist_search_mode) {
	case GCL_SEARCH_REW:
	case GCL_SEARCH_BEG:
		return search_history_func(cl, avance, begin, history_prev_to_first);

	case GCL_SEARCH_FWD:
		return search_history_func(cl, avance, begin, history_next_to_last);

	default:
		return -1;
	}
}

static void search_off(GtkCompletionLine* cl) {
	int pos = gtk_editable_get_position(GTK_EDITABLE(cl));
	gtk_editable_select_region(GTK_EDITABLE(cl), pos, pos);
	if(cl->hist_search_mode == GCL_SEARCH_FWD)
		history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),FALSE);
	else
		history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),TRUE);
	cl->hist_search_mode = GCL_SEARCH_OFF;
	g_signal_emit_by_name(G_OBJECT(cl), "search_mode");
}

#define STOP_PRESS \
	(g_signal_stop_emission_by_name(G_OBJECT(cl),	"key_press_event"))
#define MODE_BEG \
	(cl->hist_search_mode == GCL_SEARCH_BEG)
#define MODE_REW \
	(cl->hist_search_mode == GCL_SEARCH_REW)
#define MODE_FWD \
	(cl->hist_search_mode == GCL_SEARCH_FWD)
#define MODE_SRC \
	(cl->hist_search_mode != GCL_SEARCH_OFF)

static gint tab_pressed(GtkCompletionLine* cl) {
	if (MODE_SRC)
		search_off(cl);
	complete_line(cl);
	return FALSE;
}

static void clear_selection(GtkCompletionLine* cl) {
	int pos = gtk_editable_get_position(GTK_EDITABLE(cl));
	gtk_editable_select_region(GTK_EDITABLE(cl), pos, pos);
}

static gboolean on_key_press(GtkCompletionLine *cl, GdkEventKey *event, gpointer data) {
	static gint tt_id = -1;

	if (event->type == GDK_KEY_PRESS) {
		switch (event->keyval) {
		case GDK_KEY_Control_R:
		case GDK_KEY_Control_L:
		case GDK_KEY_Shift_R:
		case GDK_KEY_Shift_L:
		case GDK_KEY_Alt_R:
		case GDK_KEY_Alt_L:
			break;

		case GDK_KEY_Tab:
			if (tt_id != -1) {
				g_source_remove(tt_id);
				tt_id = -1;
			}
			tab_pressed(cl);
			STOP_PRESS;
			return TRUE;

		case GDK_KEY_Up:
			if (cl->win_compl != NULL) {
				gboolean valid;
#ifdef HAVE_GTK3
				valid = gtk_tree_model_iter_previous(cl->sort_list_compl, &(cl->list_compl_it));
#else
				int pos;
				gtk_tree_model_get(cl->sort_list_compl, &(cl->list_compl_it), 1, &pos, -1);
				if(pos!=0)
					pos--;
				else
					pos = cl->list_compl_nr_rows - 1;
				valid = gtk_tree_model_iter_nth_child(cl->sort_list_compl, &(cl->list_compl_it), NULL, pos);
#endif
				if(!valid)
					gtk_tree_model_iter_nth_child(cl->sort_list_compl, &(cl->list_compl_it), NULL, cl->list_compl_nr_rows - 1);
				complete_from_list(cl);
			} else {
				up_history(cl);
			}
			if (MODE_SRC) {
				search_off(cl);
			}
			STOP_PRESS;
			return TRUE;

		case GDK_KEY_space:
		{
			cl->first_key = 0;
			gboolean search = MODE_SRC;
			if (search)
				search_off(cl);
			if (cl->win_compl != NULL) {
				gtk_widget_destroy(cl->win_compl);
				cl->win_compl = NULL;
				if (!search) {
					int pos = gtk_editable_get_position(GTK_EDITABLE(cl));
					gtk_editable_select_region(GTK_EDITABLE(cl), pos, pos);
				}
			}
		}
		return FALSE;

		case GDK_KEY_Down:
			if (cl->win_compl != NULL) {
				gboolean valid =
					gtk_tree_model_iter_next(cl->sort_list_compl, &(cl->list_compl_it));
				if(!valid)
					gtk_tree_model_get_iter_first(cl->sort_list_compl, &(cl->list_compl_it));
				complete_from_list(cl);
			} else {
				down_history(cl);
			}
			if (MODE_SRC) {
				search_off(cl);
			}
			STOP_PRESS;
			return TRUE;

		case GDK_KEY_Return:
			if (cl->win_compl != NULL) {
				gtk_widget_destroy(cl->win_compl);
				cl->win_compl = NULL;
			}
			if (event->state & GDK_CONTROL_MASK) {
				g_signal_emit_by_name(G_OBJECT(cl), "runwithterm");
			} else {
				g_signal_emit_by_name(G_OBJECT(cl), "activate");
			}
			STOP_PRESS;
			return TRUE;

		case GDK_KEY_exclam:
			if (!MODE_BEG) {
				if (!MODE_SRC)
					gtk_editable_delete_selection(GTK_EDITABLE(cl));
				const char *tmp = gtk_entry_get_text(GTK_ENTRY(cl));
				if (!(*tmp == '\0' || cl->first_key))
					goto ordinary;
				cl->hist_search_mode = GCL_SEARCH_BEG;
				g_string_set_size(cl->hist_word, 0);
				g_signal_emit_by_name(G_OBJECT(cl), "search_mode");
				STOP_PRESS;
				return TRUE;
			} else goto ordinary;

		case GDK_KEY_R:
		case GDK_KEY_r:
			if (event->state & GDK_CONTROL_MASK) {
				if (MODE_SRC) {
					search_history_func(cl, TRUE, MODE_BEG, history_prev_to_first);
				} else {
					cl->hist_search_mode = GCL_SEARCH_REW;
					g_string_set_size(cl->hist_word, 0);
					history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),TRUE);
					g_signal_emit_by_name(G_OBJECT(cl), "search_mode");
				}
				STOP_PRESS;
				return TRUE;
			} else goto ordinary;

		case GDK_KEY_S:
		case GDK_KEY_s:
			if (event->state & GDK_CONTROL_MASK) {
				if (MODE_SRC) {
					search_history_func(cl, TRUE, MODE_BEG, history_next_to_last);
				} else {
					cl->hist_search_mode = GCL_SEARCH_FWD;
					g_string_set_size(cl->hist_word, 0);
					history_reset_position(gtk_entry_get_text(GTK_ENTRY(cl)),FALSE);
					g_signal_emit_by_name(G_OBJECT(cl), "search_mode");
				}
				STOP_PRESS;
				return TRUE;
			} else goto ordinary;

		case GDK_KEY_BackSpace:
			if (MODE_SRC) {
				if (cl->hist_word->len > 0) {
					cl->hist_word = g_string_truncate(cl->hist_word, cl->hist_word->len - 1);
					g_signal_emit_by_name(G_OBJECT(cl), "search_letter");
				}
				STOP_PRESS;
				return TRUE;
			}
			return FALSE;

		case GDK_KEY_Home:
		case GDK_KEY_End:
			clear_selection(cl);
			goto ordinary;

		case GDK_KEY_Escape:
			if (MODE_SRC) {
				search_off(cl);
			} else if (cl->win_compl != NULL) {
				gtk_widget_destroy(cl->win_compl);
				cl->win_compl = NULL;
			} else {
				// user cancelled
				g_signal_emit_by_name(G_OBJECT(cl), "cancel");
			}
			STOP_PRESS;
			return TRUE;

		case GDK_KEY_G:
		case GDK_KEY_g:
			if (event->state & GDK_CONTROL_MASK) {
				search_off(cl);
				if (MODE_SRC)
					STOP_PRESS;
				return TRUE;
			} else goto ordinary;

		case GDK_KEY_E:
		case GDK_KEY_e:
			if (event->state & GDK_CONTROL_MASK) {
				search_off(cl);
				if (MODE_SRC)
					clear_selection(cl);
			}
			goto ordinary;

		ordinary:
		default:
			cl->first_key = 0;
			if (cl->win_compl != NULL) {
				gtk_widget_destroy(cl->win_compl);
				cl->win_compl = NULL;
			}
			cl->where = NULL;
			if (MODE_SRC) {
				if (event->length > 0) {
					cl->hist_word = g_string_append(cl->hist_word, event->string);
					if (search_history(cl, FALSE, MODE_BEG) <= 0)
						cl->hist_word = g_string_truncate(cl->hist_word, cl->hist_word->len - 1);
					STOP_PRESS;
					return TRUE;
				} else
					search_off(cl);
			}
			if (cl->tabtimeout != 0) {
				if (tt_id != -1) {
					g_source_remove(tt_id);
					tt_id = -1;
				}
				if (isprint(*event->string))
					tt_id = g_timeout_add(cl->tabtimeout, (GSourceFunc)(tab_pressed), cl);
			}
			break;
		}
	}
	return FALSE;
}

#undef STOP_PRESS
#undef MODE_BEG
#undef MODE_REW
#undef MODE_FWD
#undef MODE_SRC
/* vim: set noexpandtab: */

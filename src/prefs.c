/*****************************************************************************
 *	$Id: prefs.c
 *	Copyright (C) 2000, Mishoo
 *	Author: Mihai Bazon			Email: mishoo@fenrir.infoiasi.ro
 *	Maintainer: Samuel Bauer	Email: samuel.bauer@yahoo.fr
 *
 *	Distributed under the terms of the GNU General Public License. You are
 *	free to use/modify/distribute this program as long as you comply to the
 *	terms of the GNU General Public License, version 2 or above, at your
 *	option, and provided that this copyright notice remains intact.
 *****************************************************************************/

#define GMRUNRC "gmrun.ini"

#include "prefs.h"
#include "config.h"

GKeyFile *configuration; /* Main configuration key file */

/** \brief Set undefined keys to default values
 * \return undefined values count */
int prefs_set_default()
{
	int modified = 0;
	if(!g_key_file_has_key(configuration, "Terminal", "command", NULL) && ++modified)
		g_key_file_set_string(configuration, "Terminal", "command", "gnome-terminal");
	if(!g_key_file_has_key(configuration, "Terminal", "exec", NULL) && ++modified)
		g_key_file_set_string(configuration, "Terminal", "exec", "-e");
	if(!g_key_file_has_key(configuration, "Terminal", "always_in_terminal", NULL) && ++modified) {
		gchar **list = g_strsplit("ssh telnet ftp lynx mc vi vim pine centericq perldoc man", " ", -1);
		g_key_file_set_string_list(configuration, "Terminal", "always_in_terminal", (const gchar **)list, g_strv_length(list));
		g_strfreev(list);
	}
	if(!g_key_file_has_key(configuration, "Window", "width", NULL) && ++modified)
		g_key_file_set_integer(configuration, "Window", "width", 400);
	if(!g_key_file_has_key(configuration, "Window", "top", NULL) && ++modified)
		g_key_file_set_integer(configuration, "Window", "top", 100);
	if(!g_key_file_has_key(configuration, "History", "lines", NULL) && ++modified)
		g_key_file_set_integer(configuration, "History", "lines", 256);
	if(!g_key_file_has_key(configuration, "History", "show", NULL) && ++modified)
		g_key_file_set_boolean(configuration, "History", "show", TRUE);
	if(!g_key_file_has_key(configuration, "History", "ignorespace", NULL) && ++modified)
		g_key_file_set_boolean(configuration, "History", "ignorespace", FALSE);
	if(!g_key_file_has_key(configuration, "History", "empty_end", NULL) && ++modified)
		g_key_file_set_boolean(configuration, "History", "empty_end", TRUE);
	if(!g_key_file_has_key(configuration, "Completion", "show_hidden", NULL) && ++modified)
		g_key_file_set_boolean(configuration, "Completion", "show_hidden", FALSE);
	if(!g_key_file_has_key(configuration, "Completion", "timeout", NULL) && ++modified)
		g_key_file_set_integer(configuration, "Completion", "timeout", 0);
	return modified;
}

/** \brief Initialize configuration */
void prefs_init() {
	gchar *filename = g_build_filename (g_get_user_config_dir (), "gmrun.ini", NULL);

	/* TODO: never freed */
	configuration = g_key_file_new ();
	g_key_file_load_from_file (configuration, filename, G_KEY_FILE_NONE, NULL);
	if(prefs_set_default() > 0)
		g_key_file_save_to_file (configuration, filename, NULL);

	g_free(filename);
}
/* vim: set noexpandtab: */

gmrun
=====

https://gitlab.com/mazes_80/gmrun

Introduction
------------

Short GtkEntry for file autocompletion that does the needed stuff for running programs. This is intended as a replacement to grun or gnome-run. Inspired by ALT-F2 under KDE nearly two decades ago.
    
    Copyright (c) 2000-2003 Mihai Bazon
    Author: Mihai Bazon <mishoo@infoiasi.ro>
    Maintainer: mazes_80 <samuel.bauer@yahoo.fr>
    
send postcards to:

    Mihai Bazon,
    str. Republicii, nr. 350, sc. E, ap. 9, cod 6500 VASLUI - VASLUI
	Romania

This program falls under the GNU General Public License version 2 or above.


Features
--------

* Tilde completion, completion for multiple words.
* History file 
* Configuration file
* URL handlers
* Extension handlers
* UTF-8 support

Keys
----

1. TAB: begins a completion
2. Esc: close the window or completion field if opened
3. Home/End: Close completion field and clear selmection
4. Enter: Execute command in the entry
5. Ctrl+Enter: Execute command in a terminal (set your choice in preferences)
6. Ctrl+R/S: Search in history, backward or forward. (Searches beginning by ! will only match line beginning by pattern)

Requirements
------------

* GTK+ 3, gtk+2 can also be used through configure switch

Install
-------

Use the configure script:

    $ ./configure
    $ make
    $ make install

After this the executable goes usually in /usr/local/bin, make sure this is in your path.
    
Configuration
-------------

* Terminal
	- command: terminal emulator and favourite options
	- exec: The option to execute something inside (mostly -e)
	- always\_in\_terminal: Application list to be run inside a terminal emulator, like mc, top ... (semicolon separated)
* Window
	- width: Window width
	- top: windows position y
* History
	- lines: Number of line to store in history
	- show: Should last command appear when launching gmrun
	- ignorespace: Should line beginning by blank be ignored ?
	- empty\_end: is command list terminated by an empty line
* Completion
	- show\_hidden: Complete hidden/dot files
	- timeout: 0 to disactivate, time in ms before autolaunch

Tips and tricks
---------------
    
1. Use xbindkeys a wm agnostic key binder

Bugs
----
* Documentation is inexistent. As it gives headache to maintainer, current maintainer tried to comment a bit.
* Please report any bugs or features.

Foreword
--------

The code for completion was originally written in C++, it is now using glib helpers. Original author claimed that would take a full C rewrite, but it did not. Effectively the source code is a bit smaller, and executable should be faster, as allocations count for copy dropped.

This piece of code is not intended to be used as a widget, only as standalone application, as static variables are used in the gtk class.

Disclaimer
----------

* The Short Way: NO WARRANTIES OF ANY KIND.  USE IT AT YOUR OWN RISK.
* The Right Way: Please read the GNU General Public License.  This program falls under its terms.

(: END OF TERMS AND CONDITIONS :)
